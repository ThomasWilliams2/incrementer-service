# Incrementer Service

## Requirements

- node
- npm
- docker

## Steps to run

- clone repository
- install dependencies
  - `npm i`
- start postgres and redis, via docker
  - `npm run db:up`
  - afterwards, when everything is done, run `npm run db:down` to stop those containers
- run database migrations
  - `npm run migrate`
- start server
  - `npm run dev`
- make a request
  - `curl -d '{ "key": "foo", "value": 10 }' -H "Content-Type: application/json" -X POST localhost:3333/increment`

## Tests

- unit tests
  - `npm run test`
- benchmarks
  - `npm run test:benchmark`

### Benchmark Results

```
┌─────────┬──────┬───────┬───────┬───────┬──────────┬─────────┬────────┐
│ Stat    │ 2.5% │ 50%   │ 97.5% │ 99%   │ Avg      │ Stdev   │ Max    │
├─────────┼──────┼───────┼───────┼───────┼──────────┼─────────┼────────┤
│ Latency │ 5 ms │ 12 ms │ 23 ms │ 33 ms │ 12.69 ms │ 7.74 ms │ 155 ms │
└─────────┴──────┴───────┴───────┴───────┴──────────┴─────────┴────────┘
┌───────────┬────────┬────────┬────────┬────────┬────────┬─────────┬────────┐
│ Stat      │ 1%     │ 2.5%   │ 50%    │ 97.5%  │ Avg    │ Stdev   │ Min    │
├───────────┼────────┼────────┼────────┼────────┼────────┼─────────┼────────┤
│ Req/Sec   │ 606    │ 606    │ 762    │ 899    │ 750.7  │ 88.05   │ 606    │
├───────────┼────────┼────────┼────────┼────────┼────────┼─────────┼────────┤
│ Bytes/Sec │ 109 kB │ 109 kB │ 137 kB │ 162 kB │ 135 kB │ 15.9 kB │ 109 kB │
└───────────┴────────┴────────┴────────┴────────┴────────┴─────────┴────────┘

Req/Bytes counts sampled once per second.

8k requests in 10.02s, 1.35 MB read
```
