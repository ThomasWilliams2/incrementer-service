export interface IncrementPayload {
  key: string;
  value: number;
}

export interface IncrementerJobPayload extends IncrementPayload {
  timestamp: number;
}
