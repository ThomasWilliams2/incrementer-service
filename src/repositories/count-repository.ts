import { PrismaClient } from "@prisma/client";
import { inject, injectable } from "inversify";
import { SERVICE_IDENTIFIERS } from "../constants";

@injectable()
export class CountRepository {
  constructor(
    @inject(SERVICE_IDENTIFIERS.prismaClient) private prisma: PrismaClient
  ) {}

  async incrementCountForKey(key: string, value: number): Promise<number> {
    const record = await this.prisma.count.upsert({
      where: { key },
      create: { key, value },
      update: { key, value: { increment: value } },
    });
    return record.id;
  }
}
