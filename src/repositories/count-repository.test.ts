import { PrismaClient } from "@prisma/client";
import { CountRepository } from "./count-repository";
import { mockDeep, mockReset, any, anyObject } from "jest-mock-extended";
import { DeepMockProxy } from "jest-mock-extended/lib/mjs/Mock";

describe("CountRepository", () => {
  const testId = 12345;
  let mockPrismaClient: DeepMockProxy<PrismaClient>;
  let uut: CountRepository;

  beforeAll(() => {
    mockPrismaClient = mockDeep<PrismaClient>();
    uut = new CountRepository(mockPrismaClient);
  });

  beforeEach(() => {
    mockPrismaClient.count.upsert
      .calledWith(anyObject())
      .mockResolvedValue({ id: testId, key: "", value: 0 });
  });

  afterEach(() => {
    mockReset(mockPrismaClient);
  });

  describe("incrementCountForKey", () => {
    it("should call upsert on the count table", async () => {
      await uut.incrementCountForKey("key", 5);
      expect(mockPrismaClient.count.upsert).toHaveBeenCalledTimes(1);
    });

    it("should return the id from the db operation", async () => {
      const returnValue = await uut.incrementCountForKey("asdf", 5);
      expect(returnValue).toEqual(testId);
    });
  });
});
