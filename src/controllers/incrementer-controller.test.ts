import { mock, mockReset, MockProxy } from "jest-mock-extended";
import { IncrementerController } from "./incrementer-controller";
import { IncrementerService } from "../services/incrementer-service";
import { FastifyRequest, FastifyReply } from "fastify";
import { IncrementPayload } from "../interfaces";

jest.mock("fastify");
jest.mock("../services/incrementer-service");

describe("HelloWorldController", () => {
  let mockIncrementerService: MockProxy<IncrementerService>;
  let uut: IncrementerController;

  beforeAll(() => {
    mockIncrementerService = mock<IncrementerService>();
    uut = new IncrementerController(mockIncrementerService);
  });

  beforeEach(() => {
    mockReset(mockIncrementerService);
  });

  describe("handle", () => {
    const testBody = { key: "key", value: 5 };
    const request: MockProxy<FastifyRequest<{ Body: IncrementPayload }>> =
      mock<FastifyRequest<{ Body: IncrementPayload }>>();
    request.body = testBody;

    const reply: MockProxy<FastifyReply> = mock<FastifyReply>();

    it("should call incrementByKeyValue", async () => {
      await uut.handle(request, reply);
      expect(mockIncrementerService.incrementKeyByValue).toHaveBeenCalledTimes(
        1
      );
      expect(mockIncrementerService.incrementKeyByValue).toHaveBeenCalledWith(
        testBody
      );
    });

    it("should send a reply", async () => {
      await uut.handle(request, reply);
      expect(reply.send).toHaveBeenCalled();
    });
  });
});
