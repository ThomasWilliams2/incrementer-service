import { inject, injectable } from "inversify";
import { HelloWorldService } from "../services/hello-world-service";

@injectable()
export class HelloWorldController {
  constructor(@inject(HelloWorldService) private service: HelloWorldService) {}

  handle() {
    return this.service.getHelloWorldReply();
  }
}
