import { FastifyRequest, FastifyReply } from "fastify";
import { inject, injectable } from "inversify";
import { IncrementPayload } from "../interfaces";
import { IncrementerService } from "../services/incrementer-service";

@injectable()
export class IncrementerController {
  constructor(
    @inject(IncrementerService) private service: IncrementerService
  ) {}

  async handle(
    req: FastifyRequest<{ Body: IncrementPayload }>,
    rep: FastifyReply
  ) {
    const { key, value } = req.body;
    await this.service.incrementKeyByValue({ key, value });
    rep.code(202);
    rep.send("job queued");
  }
}
