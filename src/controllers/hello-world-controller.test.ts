import { mock, mockReset, MockProxy } from "jest-mock-extended";
import { HelloWorldController } from "./hello-world-controller";
import { HelloWorldService } from "../services/hello-world-service";

jest.mock("../services/hello-world-service");

describe("HelloWorldController", () => {
  const returnValue = { hello: "asdf" };
  let mockHelloWorldService: MockProxy<HelloWorldService>;
  let uut: HelloWorldController;

  beforeAll(() => {
    mockHelloWorldService = mock<HelloWorldService>();
    uut = new HelloWorldController(mockHelloWorldService);
  });

  beforeEach(() => {
    mockHelloWorldService.getHelloWorldReply.mockReturnValue(
      Promise.resolve(returnValue)
    );
  });

  afterEach(() => {
    mockReset(mockHelloWorldService);
  });

  describe("handle", () => {
    it("should call getHelloWorldReply", async () => {
      await uut.handle();
      expect(mockHelloWorldService.getHelloWorldReply).toHaveBeenCalledTimes(1);
    });

    it("should return the value from the service", async () => {
      const value = await uut.handle();
      expect(value).toBe(returnValue);
    });
  });
});
