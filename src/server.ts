import { FastifyInstance } from "fastify";
import { inject, injectable } from "inversify";
import { SERVICE_IDENTIFIERS } from "./constants";
import { HelloWorldController } from "./controllers/hello-world-controller";
import { IncrementerController } from "./controllers/incrementer-controller";
import { IncrementPayload } from "./interfaces";

@injectable()
export class Server {
  constructor(
    @inject(SERVICE_IDENTIFIERS.fastifyInstance)
    private fastify: FastifyInstance,
    @inject(SERVICE_IDENTIFIERS.httpPort) private httpPort: number,
    @inject(HelloWorldController)
    private helloWorldController: HelloWorldController,
    @inject(IncrementerController)
    private incrementerController: IncrementerController
  ) {}

  bindRoutes() {
    this.fastify.get("/", async (req, rep) =>
      this.helloWorldController.handle()
    );

    this.fastify.post<{ Body: IncrementPayload }>(
      "/increment",
      async (req, rep) => this.incrementerController.handle(req, rep)
    );
  }

  async start() {
    this.bindRoutes();
    try {
      await this.fastify.listen(this.httpPort);
      console.log(`Server listening on port ${this.httpPort}`);
    } catch (err) {
      console.error(err);
      process.exit(1);
    }
  }
}
