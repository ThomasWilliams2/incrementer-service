import { Container } from "inversify";
import Queue from "bee-queue";
import { PrismaClient } from "@prisma/client";
import { SERVICE_IDENTIFIERS } from "./constants";
import { IncrementPayload } from "./interfaces";
import Fastify, { FastifyInstance } from "fastify";

const DEFAULT_REDIS_HOST = "localhost";
const DEFAULT_REDIS_PORT = 6379;
const DEFAULT_HTTP_PORT = 3333;

const container = new Container({
  autoBindInjectable: true,
});

container
  .bind<number>(SERVICE_IDENTIFIERS.httpPort)
  .toConstantValue(parseInt(process.env.HTTP_PORT ?? "") || DEFAULT_HTTP_PORT);

container
  .bind<Queue<IncrementPayload>>(SERVICE_IDENTIFIERS.incrementerQueue)
  .toConstantValue(
    new Queue("incrementer", {
      redis: {
        host: process.env.REDIS_HOST ?? DEFAULT_REDIS_HOST,
        port: parseInt(process.env.REDIS_PORT ?? "") || DEFAULT_REDIS_PORT,
      },
    })
  );

container
  .bind<PrismaClient>(SERVICE_IDENTIFIERS.prismaClient)
  .toConstantValue(new PrismaClient());

container
  .bind<FastifyInstance>(SERVICE_IDENTIFIERS.fastifyInstance)
  .toConstantValue(Fastify());

export { container };
