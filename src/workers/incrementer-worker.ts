import { inject, injectable } from "inversify";
import Queue, { Job } from "bee-queue";
import { SERVICE_IDENTIFIERS } from "../constants";
import { IncrementPayload, IncrementerJobPayload } from "../interfaces";
import { CountRepository } from "../repositories/count-repository";

@injectable()
export class IncrementerWorker {
  constructor(
    @inject(SERVICE_IDENTIFIERS.incrementerQueue)
    private incrementerQueue: Queue<IncrementerJobPayload>,
    @inject(CountRepository) private countRepository: CountRepository
  ) {}

  async handleJob(job: Job<IncrementerJobPayload>) {
    const { key, value, timestamp } = job.data;
    const id = await this.countRepository.incrementCountForKey(key, value);
    console.log(
      `Record with key ${key} incremented by ${value} in ${
        Date.now() - timestamp
      } milliseconds`
    );
    return id;
  }

  run() {
    this.incrementerQueue.process<number>((job) => this.handleJob(job));
  }
}
