import Queue, { Job } from "bee-queue";
import { mock, MockProxy, mockReset } from "jest-mock-extended";
import { IncrementPayload, IncrementerJobPayload } from "../interfaces";
import { CountRepository } from "../repositories/count-repository";
import { IncrementerWorker } from "./incrementer-worker";

describe("IncrementerWorker", () => {
  let mockIncrementerQueue: MockProxy<Queue<IncrementerJobPayload>>;
  let mockIncrementerQueueJob: MockProxy<Job<IncrementerJobPayload>>;
  let mockCountRepository: MockProxy<CountRepository>;
  let uut: IncrementerWorker;

  beforeAll(() => {
    mockIncrementerQueue = mock<Queue<IncrementerJobPayload>>();
    mockIncrementerQueueJob = mock<Job<IncrementerJobPayload>>();
    mockCountRepository = mock<CountRepository>();
    uut = new IncrementerWorker(mockIncrementerQueue, mockCountRepository);
  });

  afterEach(() => {
    mockReset(mockIncrementerQueue);
    mockReset(mockCountRepository);
  });

  describe("run", () => {
    it("should process a job from the queue", () => {
      uut.run();
      expect(mockIncrementerQueue.process).toHaveBeenCalledTimes(1);
    });
  });

  describe("handleJob", () => {
    it("should increment the count with the repository", async () => {
      const key = "foobar";
      const value = 6;
      mockIncrementerQueueJob.data = { key, value, timestamp: Date.now() };

      await uut.handleJob(mockIncrementerQueueJob);
      expect(mockCountRepository.incrementCountForKey).toHaveBeenCalledTimes(1);
      expect(mockCountRepository.incrementCountForKey).toHaveBeenCalledWith(
        key,
        value
      );
    });

    it("should return the entity received from the repository", async () => {
      const testId = 9999;
      const key = "foobar";
      const value = 6;
      mockIncrementerQueueJob.data = { key, value, timestamp: Date.now() };
      mockCountRepository.incrementCountForKey
        .calledWith(key, value)
        .mockResolvedValue(testId);

      const returnId = await uut.handleJob(mockIncrementerQueueJob);
      expect(returnId).toEqual(testId);
    });
  });
});
