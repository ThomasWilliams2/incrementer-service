export const SERVICE_IDENTIFIERS = {
  httpPort: Symbol("http-port"),
  incrementerQueue: Symbol("incrementer-queue"),
  prismaClient: Symbol("prisma-client"),
  fastifyInstance: Symbol("fastify-instance"),
};
