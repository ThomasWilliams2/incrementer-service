import Queue from "bee-queue";
import { inject, injectable } from "inversify";
import { SERVICE_IDENTIFIERS } from "../constants";
import { IncrementPayload, IncrementerJobPayload } from "../interfaces";

@injectable()
export class IncrementerService {
  constructor(
    @inject(SERVICE_IDENTIFIERS.incrementerQueue)
    private incrementerQueue: Queue<IncrementerJobPayload>
  ) {}

  async incrementKeyByValue({ key, value }: IncrementPayload) {
    const incremementJob = this.incrementerQueue.createJob({
      key,
      value,
      timestamp: Date.now(),
    });
    return incremementJob.retries(2).save(() => {
      console.log(`job queued with key: ${key}, value: ${value}`);
    });
  }
}
