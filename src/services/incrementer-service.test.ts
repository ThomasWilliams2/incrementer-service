import Queue, { Job } from "bee-queue";
import { IncrementerService } from "./incrementer-service";
import {
  mock,
  mockReset,
  MockProxy,
  anyObject,
  anyNumber,
} from "jest-mock-extended";
import { IncrementerJobPayload } from "../interfaces";

describe("IncrementerService", () => {
  let mockIncrementerQueue: MockProxy<Queue<IncrementerJobPayload>>;
  let mockIncrementerQueueJob: MockProxy<Job<IncrementerJobPayload>>;
  let uut: IncrementerService;

  beforeAll(() => {
    mockIncrementerQueue = mock<Queue<IncrementerJobPayload>>();
    mockIncrementerQueueJob = mock<Job<IncrementerJobPayload>>();
    uut = new IncrementerService(mockIncrementerQueue);
  });

  beforeEach(() => {
    mockIncrementerQueue.createJob
      .calledWith(anyObject())
      .mockReturnValue(mockIncrementerQueueJob);
    mockIncrementerQueueJob.retries
      .calledWith(anyNumber())
      .mockReturnValue(mockIncrementerQueueJob);
  });

  afterEach(() => {
    mockReset(mockIncrementerQueue);
  });

  describe("incrementKeyByValue", () => {
    it("should add a job to the queue", async () => {
      const payload = { key: "foo", value: 7 };
      await uut.incrementKeyByValue(payload);

      expect(mockIncrementerQueue.createJob).toHaveBeenCalledTimes(1);
      expect(mockIncrementerQueue.createJob).toHaveBeenCalledWith(payload);
    });
  });
});
