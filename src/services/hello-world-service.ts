import { injectable } from "inversify";

@injectable()
export class HelloWorldService {
  async getHelloWorldReply() {
    return { hello: "World" };
  }
}
