import "reflect-metadata";
import throng from "throng";
import { IncrementerWorker } from "./workers/incrementer-worker";
import { container } from "./container";

const incrementerWorker = container.get(IncrementerWorker);

const worker = () => {
  incrementerWorker.run();
};

throng({ worker });
