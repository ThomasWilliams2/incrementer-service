import autocannon from "autocannon";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const printResult = require("autocannon/lib/printResult");

const generateRequestBody = () => {
  const key = String.fromCharCode(65 + Math.floor(Math.random() * 26));
  const value = Math.floor(Math.random() * 10);
  return JSON.stringify({ key, value });
};

const bench = autocannon(
  {
    url: "http://localhost:3333",
    requests: [
      {
        method: "POST",
        path: "/increment",
        headers: {
          "content-type": "application/json",
        },
        body: generateRequestBody(),
      },
    ],
  },
  (err, res) => {
    if (err) {
      console.error("There was an issue with the benchmark", err);
      process.exit(1);
    }
    console.log("benchmark finished");
    console.log(printResult(res));
    process.exit(0);
  }
);

bench.on("response", (client) => {
  client.setBody(generateRequestBody());
});
